package ticket.ispider.com.CoreModules;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;

public class Application extends AppCompatActivity {

    int layout;

    Application(int content_layout)
    {
        this.layout = content_layout;

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout);


    }

    @Override
    public void onBackPressed() {


    }


}//END
