package ticket.ispider.com.CoreModules.Team_Leader;

import android.os.Bundle;
import android.provider.Contacts;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ticket.ispider.com.CoreModules.Coordinator.Masters;
import ticket.ispider.com.CoreModules.Field_Engineer.My_Tickets;
import ticket.ispider.com.CoreModules.Generic_DataObjects;
import ticket.ispider.com.R;
import ticket.ispider.com.Utility.GenericRecyclerAdapter;

public class Ticket_Approve extends AppCompatActivity {

    //**********************************************************************************************
    private ActionBar actionBar;
    private Toolbar toolbar;
    @BindView(R.id.rcylr_list)
    RecyclerView rcylrview;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.screen_ticket_approve);
        setContentView(R.layout.screen_mytickets_main);

        try {
            GET_INITIALIZE();
            CONTROLLISTENERS();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void CONTROLLISTENERS() {

        rcylrview.setLayoutManager(new LinearLayoutManager(Ticket_Approve.this));
        rcylrview.setNestedScrollingEnabled(false);

        datalist=new ArrayList<>();
        Generic_DataObjects.Ticket_Approve value= new Generic_DataObjects.Ticket_Approve();

        for (int i = 0; i < 10; i++) {

            value= new Generic_DataObjects.Ticket_Approve();
            value.setId(i+1);
            value.setCallCloseDate("02-09-2018");
            value.setCallOpenDate("01-09-2018");
            value.setProblemReported("hello test");
            value.setServerId("2");
            value.setTicketNo("1123");
            datalist.add(value);
        }

        LoadList();

    }



    //Recycler data
    GenericRecyclerAdapter RecyclerAdapter;
    List<Generic_DataObjects.Ticket_Approve> datalist = new ArrayList<>();

    private void LoadList() {


        RecyclerAdapter = new GenericRecyclerAdapter(datalist, R.layout.rowitem_ticket_approve_carditem)
                .setRowItemView(new GenericRecyclerAdapter.AdapterView() {
                    @Override
                    public Object setAdapterView(ViewGroup parent, int viewType, int layoutId) {
                        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
                    }


                    @Override
                    public void onBindView(Object holder, final int position, Object data, final List<Object> dataList) {

                        ViewHolder myViewHolders = (ViewHolder) holder;
                        final Generic_DataObjects.Ticket_Approve value = (Generic_DataObjects.Ticket_Approve) data;

                        try {

                          myViewHolders.txtvwSno.setText(String.valueOf(value.getId()));
                          myViewHolders.txtvwTicketno.setText(value.getTicketNo());
                          myViewHolders.txtvwCallopendate.setText(value.getCallOpenDate());
                          myViewHolders.txtvwCallclosedate.setText(value.getCallCloseDate());
                          myViewHolders.txtvwProblemreported.setText(value.getProblemReported());
                            myViewHolders.imgvwOptions.setVisibility(View.GONE);
                          myViewHolders.imgvwOptions.setOnClickListener(new View.OnClickListener() {
                              @Override
                              public void onClick(View v) {

                                  PopupMenu popupMenu = new PopupMenu(Ticket_Approve.this, v);
                                  popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                      @Override
                                      public boolean onMenuItemClick(MenuItem item) {

                                          if(item.getTitle().toString().equalsIgnoreCase("Approve"))
                                          {

                                          }

                                          return true;
                                      }
                                  });
                                  popupMenu.inflate(R.menu.menu_ticket_approve_more);
                                  popupMenu.show();
                              }
                          });


                          myViewHolders.parentLayout.setOnClickListener(new View.OnClickListener() {
                              @Override
                              public void onClick(View v) {

                                  AlertDialog.Builder builder = new AlertDialog.Builder(Ticket_Approve.this);
                                  LayoutInflater inflater = Ticket_Approve.this.getLayoutInflater();
                                  View inflatedLayout = inflater.inflate(R.layout.screen_ticket_approve, null);

                                  AlertDialog show;

                                  // Set the dialog layout
                                  builder.setView(inflatedLayout);
                                  show = builder.show();
                                  show.setCancelable(false);
                                  show.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation2;

                                  MaterialRippleLayout buttonCancel = (MaterialRippleLayout) inflatedLayout.findViewById(R.id.button_cancel);
                                  MaterialRippleLayout buttonSave = (MaterialRippleLayout) inflatedLayout.findViewById(R.id.button_save);


                                  buttonCancel.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          show.dismiss();
                                      }
                                  });

                              }
                          });



                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                });
        rcylrview.setAdapter(RecyclerAdapter);



    }




    public static class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout parentLayout;
        private LinearLayout lytParent;
        private TextView txtvwSno;
        private TextView txtvwTicketno;
        private TextView txtvwCallopendate;
        private TextView txtvwCallclosedate;
        private TextView txtvwProblemreported;
        private ImageView imgvwOptions;


        ViewHolder(View view) {
            super(view);
            parentLayout = (LinearLayout) view.findViewById(R.id.parent_layout);
            lytParent = (LinearLayout) view.findViewById(R.id.lyt_parent);
            txtvwSno = (TextView) view.findViewById(R.id.txtvw_sno);
            txtvwTicketno = (TextView) view.findViewById(R.id.txtvw_ticketno);
            txtvwCallopendate = (TextView) view.findViewById(R.id.txtvw_callopendate);
            txtvwCallclosedate = (TextView) view.findViewById(R.id.txtvw_callclosedate);
            txtvwProblemreported = (TextView) view.findViewById(R.id.txtvw_problemreported);
            imgvwOptions = (ImageView) view.findViewById(R.id.imgvw_options);
        }

    }


    //**********************************************************************************************
    private void GET_INITIALIZE() {

        try {
            ButterKnife.bind(this);
            initToolbar();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //**********************************************************************************************
    private void initToolbar() {
        try {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle("Ticket Approve");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //**********************************************************************************************
    @Override
    public void onBackPressed() {

        this.finish();

    }

    //**********************************************************************************************

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();  return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //**********************************************************************************************
    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    //**********************************************************************************************
    @Override
    protected void onStop() {
        super.onStop();
    }

    //**********************************************************************************************
}//END