package ticket.ispider.com.CoreModules.Coordinator;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.ButterKnife;
import ticket.ispider.com.R;

public class Ticket_Entry extends AppCompatActivity {


    //**********************************************************************************************
    private ActionBar actionBar;
    private Toolbar toolbar;


    //**********************************************************************************************
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_ticket_entry);

        try {
            GET_INITIALIZE();
            CONTROLLISTENERS();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    //**********************************************************************************************
    private void CONTROLLISTENERS() {



    }
    //**********************************************************************************************
    private void GET_INITIALIZE() {

        try {
            ButterKnife.bind(this);
            initToolbar();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //**********************************************************************************************
    private void initToolbar() {
        try {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle("Ticket Entry");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //**********************************************************************************************
    @Override
    public void onBackPressed() {

        this.finish();

    }

    //**********************************************************************************************

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();  return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //**********************************************************************************************
    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    //**********************************************************************************************
    @Override
    protected void onStop() {
        super.onStop();
    }

    //**********************************************************************************************
}//END
