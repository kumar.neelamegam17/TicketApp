package ticket.ispider.com.CoreModules;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ticket.ispider.com.R;
import ticket.ispider.com.Utility.CustomIntent;
import ticket.ispider.com.Utility.GenericDialog;

public class Constant {

    public Constant constants;

    public Constant getInstance() {
        if (constants == null) {
            constants = new Constant();
            return constants;
        }

        return constants;
    }




    public static void HighlightMandatory(String LabelName, TextView textview) {
        String starsymbol = "<font color='#EE0000'><b>*</b></font>";
        textview.setText(Html.fromHtml(LabelName + starsymbol));
    }


    public static String GetCurrentTime() {
        String str = null;
        try {

            str = "";
            SimpleDateFormat sdf4 = new SimpleDateFormat("h:mm a");
            String currentDateandTime = sdf4.format(new Date());     //8:29 PM
            str = currentDateandTime;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }


    public static String GetTimeStamp()
    {
        String str="";
        try {

            Long tsLong = System.currentTimeMillis()/1000;
            str = tsLong.toString();

            return str;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str;
    }

    public static void ClearError(final EditText edt_text) {

        edt_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (edt_text.getError() != null) {
                    edt_text.setError(null);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }


    public static void globalStartIntent(Context context, Class classes, Bundle bundle) {
        ((Activity) context).finish();
        Intent intent = new Intent(context, classes);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        CustomIntent.customType(context, 4);
        context.startActivity(intent);

    }

    public static void globalStartIntent2(Context context, Class classes, Bundle bundle) {

        Intent intent = new Intent(context, classes);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        CustomIntent.customType(context, 4);
        context.startActivity(intent);

    }



    public static String GetWidgetOperations(View control, int id) {
        // android.support.v7.widget.AppCompatAutoCompleteTextView
        // 05-03 19:44:20.435 17525-17525/displ.mobydocmarathi.com E/check 2:: class android.support.v7.widget.AppCompatAutoCompleteTextView
        //Log.e("check 1: ",control.getClass().getName().toString());
        // Log.e("check 2: ",control.getClass().toString());
        String str = "";

        if (control instanceof EditText) {

            EditText edt = (EditText) control;
            str = EditTextOperations(edt, id, "", "");

        } else if (control instanceof AutoCompleteTextView) {
            AutoCompleteTextView edt = (AutoCompleteTextView) control;
            str = AutoCompleteTextViewOperations(edt, id, "", "");

        } else if (control instanceof MultiAutoCompleteTextView) {
            MultiAutoCompleteTextView edt = (MultiAutoCompleteTextView) control;
            str = MultiAutoCompleteTextViewOperations(edt, id, "", "");

        } else if (control instanceof ToggleButton) {
            ToggleButton tbg = (ToggleButton) control;
            str = ToggleButtonOperations(tbg, id, "");

        }   else if (control instanceof CheckBox) {
            CheckBox chk = (CheckBox) control;
            str = CheckBoxButtonOperations(chk, id, "");

        } else if (control instanceof RadioButton) {
            RadioButton rbtn = (RadioButton) control;
            str = RadioButtonOperations(rbtn, id, "");

        } else if (control instanceof RadioGroup) {
            RadioGroup rgrp = (RadioGroup) control;
            str = RadioGroupOperations(rgrp, id, "");

        } else if (control instanceof Switch) {
            Switch swt = (Switch) control;
            str = SwitchButtonOperations(swt, id, "");

        } else if (control instanceof Spinner) {
            Spinner spn = (Spinner) control;

            str = SpinnerOperations(spn, id);

        } else if (control instanceof SeekBar) {
            SeekBar seekbar = (SeekBar) control;
            str = SeekBarOperations(seekbar, id, "", "");

        } else if (control instanceof TextView) {
            TextView txtvw = (TextView) control;
            str = TextViewOperations(txtvw, id, "", "");

        }

        return str;

    }


    public static String SeekBarOperations(SeekBar seekbar, int id, String setdata, String errormsg) {
        String str = "";
        switch (id) {
            case 1://Get text
                int p = seekbar.getProgress();
                str = String.valueOf(p);
                break;

        }

        return str;

    }


    public static String EditTextOperations(EditText edt, int id, String setdata, String errormsg) {
        String str = "";
        switch (id) {
            case 1://Get text
                str = edt.getText().toString();
                break;

            case 2://Set text
                edt.setText(setdata);
                break;

            case 3://set enable - true
                edt.setEnabled(true);
                break;

            case 4://set enable - false
                edt.setEnabled(false);
                break;

            case 5://get text length
                if (edt.getText().length() > 0) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;

            case 6://set error
                edt.setError(errormsg);
                break;
        }


        return str;

    }


    public static String TextViewOperations(TextView edt, int id, String setdata, String errormsg) {
        String str = "";
        switch (id) {
            case 1://Get text
                str = edt.getText().toString();
                break;

            case 2://Set text
                edt.setText(setdata);
                break;

            case 3://set enable - true
                edt.setEnabled(true);
                break;

            case 4://set enable - false
                edt.setEnabled(false);
                break;

            case 5://get text length
                if (edt.getText().length() > 0) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;

            case 6://set error
                edt.setError(errormsg);
                break;
        }


        return str;

    }


    public static String AutoCompleteTextViewOperations(AutoCompleteTextView edt, int id, String setdata, String errormsg) {
        String str = "";
        switch (id) {
            case 1://Get text
                str = edt.getText().toString();
                break;

            case 2://Set text
                edt.setText(setdata);
                break;

            case 3://set enable - true
                edt.setEnabled(true);
                break;

            case 4://set enable - false
                edt.setEnabled(false);
                break;

            case 5://get text length
                if (edt.getText().length() > 0) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;

            case 6://set error
                edt.setError(errormsg);
                break;

            case 7:
                edt.setThreshold(1);
                break;

        }


        return str;

    }


    public static String MultiAutoCompleteTextViewOperations(MultiAutoCompleteTextView edt, int id, String setdata, String errormsg) {
        String str = "";
        switch (id) {
            case 1://Get text
                str = edt.getText().toString();
                break;

            case 2://Set text
                edt.setText(setdata);
                break;

            case 3://set enable - true
                edt.setEnabled(true);
                break;

            case 4://set enable - false
                edt.setEnabled(false);
                break;

            case 5://get text length
                if (edt.getText().length() > 0) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;

            case 6://set error
                edt.setError(errormsg);
                break;

            case 7:
                edt.setThreshold(1);
                break;

        }
        return str;
    }


    public static String ToggleButtonOperations(ToggleButton toggleButton, int id, String setdata) {
        String str = "";
        switch (id) {
            case 1://get text
                str = toggleButton.getTextOn().toString();
                break;

            case 2://set on text
                toggleButton.setTextOn(setdata);
                break;

            case 3://set off text
                toggleButton.setTextOff(setdata);
                break;

            case 4:
                if (toggleButton.isChecked()) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;
        }

        return str;
    }


    public static String SpinnerOperations(Spinner spn, int id) {
        String str = "";
        switch (id) {
            case 1:
                if (spn.getSelectedItemPosition() > 0) {
                    str = spn.getSelectedItem().toString();
                }
                break;


        }

        return str;

    }


    public static String SwitchButtonOperations(Switch toggleButton, int id, String setdata) {
        String str = "";
        switch (id) {
            case 1://get text
                str = toggleButton.getTextOn().toString();
                break;

            case 2://set on text
                toggleButton.setTextOn(setdata);
                break;

            case 3://set off text
                toggleButton.setTextOff(setdata);
                break;

            case 4:
                if (toggleButton.isChecked()) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;
        }

        return str;
    }
    // Common methods to get all control values

    public static String RadioGroupOperations(RadioGroup rdgrp, int id, String setdata) {
        String str = "";
        switch (id) {
            case 1:
                // Get the checked Radio Button ID from Radio Group
                int selectedRadioButtonID = rdgrp.getCheckedRadioButtonId();

                // If nothing is selected from Radio Group, then it return -1
                if (selectedRadioButtonID != -1) {
                    RadioButton selectedRadioButton = (RadioButton) ((Activity) rdgrp.getContext()).findViewById(selectedRadioButtonID);
                    String selectedRadioButtonText = selectedRadioButton.getText().toString();
                    str = selectedRadioButtonText;
                } else {
                    str = "-";
                }
                break;

            case 2:
                // Get the checked Radio Button ID from Radio Group
                selectedRadioButtonID = rdgrp.getCheckedRadioButtonId();

                // If nothing is selected from Radio Group, then it return -1
                if (selectedRadioButtonID == -1) {
                    str = "false";
                } else {
                    str = "true";
                }

                break;

        }

        return str;
    }


    public static String RadioButtonOperations(RadioButton rbtn, int id, String setdata) {
        String str = "";
        switch (id) {
            case 1:
                if (rbtn.isChecked()) {
                    str = rbtn.getText().toString();
                } else {
                    str = "";
                }

                break;

            case 2:
                rbtn.setText(setdata);
                break;

            case 3:
                if (rbtn.isChecked()) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;
        }

        return str;
    }


    public static String CheckBoxButtonOperations(CheckBox rbtn, int id, String setdata) {
        String str = "";
        switch (id) {
            case 1:
                if (rbtn.isChecked()) {
                    str = rbtn.getText().toString();
                } else {
                    str = "";
                }

                break;

            case 2:
                rbtn.setText(setdata);
                break;

            case 3:
                if (rbtn.isChecked()) {
                    str = "true";
                } else {
                    str = "false";
                }
                break;
        }

        return str;
    }




    public static int GetCurrentYear() {
        // get current year、month and day
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);

        return year;
    }

    public static int GetCurrentMonth() {
        // get current year、month and day
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);

        return month;
    }


    public static int GetCurrentDay() {
        // get current year、month and day
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DATE);

        return day;
    }


    public static void loadSpinner(Spinner spinner, List<String> list) {
        try {
            spinner.setAdapter(new ArrayAdapter<String>(spinner.getContext(), android.R.layout.simple_list_item_1, list));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void loadSpinner(AutoCompleteTextView spinner, List<String> list) {
        try {
            spinner.setAdapter(new ArrayAdapter<String>(spinner.getContext(), android.R.layout.simple_list_item_1, list));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void loadSpinner(MultiAutoCompleteTextView spinner, List<String> list) {
        try {
            spinner.setAdapter(new ArrayAdapter<String>(spinner.getContext(), android.R.layout.simple_list_item_1, list));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean CheckNetwork(Context ctx) {
        ConnectivityManager cn = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        if (nf != null && nf.isConnected() == true) {

            return true;
        } else {

            return false;
        }
    }


    public static Dialog showCustomDialog(String title, String message, Context ctx) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View inflatedLayout = inflater.inflate(R.layout.popup_layout, null, false);
        Dialog builderDialog = new Dialog(ctx);
        //builderDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        TextView messageView = (TextView) inflatedLayout.findViewById(R.id.message);
        TextView titleView = (TextView) inflatedLayout.findViewById(R.id.title);


        messageView.setText(message);
        titleView.setText(title);
        builderDialog.setContentView(inflatedLayout);
        builderDialog.setCancelable(false);
        //builderDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //builderDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation2;
        //builderDialog.show();
        return builderDialog;
    }


    public static String DateFormatter(String ServerDate) {
        String return_date = "";

        //9/4/2017 6:16:09 PM
        SimpleDateFormat dateformt = new SimpleDateFormat("MMM-dd-yyyy / hh:mm a", Locale.ENGLISH);
        return_date = dateformt.format(Date.parse(ServerDate));

        return return_date;

    }

    public static String DateFormatter2(String ServerDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.ENGLISH);
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(ServerDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        String dates = spf.format(convertedDate);


        return dates;
    }

    public static String Imported_DateFormatter2(String ServerDate) {

        SimpleDateFormat dateFormat;//= new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aaa",Locale.ENGLISH);
        if (ServerDate.toString().contains("-")) {
            //"16-11-2017 06:48:21"
            dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.ENGLISH);
        } else {
            //"11-16-2017 06:48:21"
            dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aaa", Locale.ENGLISH);
        }

        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(ServerDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        String dates = spf.format(convertedDate);


        return dates;
    }

    public static String DateFormatter3(String ServerDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS", Locale.ENGLISH);
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(ServerDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        String dates = spf.format(convertedDate);


        return dates;
    }

    public static String DeviceDate() {
        String date = "";

        //2017/04/09 19:51:10
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        SimpleDateFormat dateformt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.ENGLISH);
        date = dateformt.format(Date.parse(currentDateTimeString));

        return date;
    }

    public static String Device_OnlyDate() {
        String date = "";

        try {
            //2017/04/09 19:51:10
            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            SimpleDateFormat dateformt = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
            date = dateformt.format(Date.parse(currentDateTimeString));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    public static String Device_OnlyDateMMDDYYYY() {
        String date = "";

        try {
            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            SimpleDateFormat dateformt = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
            date = dateformt.format(Date.parse(currentDateTimeString));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    public static String Device_OnlyDateWithHypon() {
        String date = "";

        try {
            //2017/04/09 19:51:10
            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            SimpleDateFormat dateformt = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            date = dateformt.format(Date.parse(currentDateTimeString));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    public static String Device_OnlyDate2() {
        String date = "";

        try {
            //2017/04/09 19:51:10
            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            SimpleDateFormat dateformt = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
            date = dateformt.format(Date.parse(currentDateTimeString));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    public static String Device_OnlyDate2WithHypon() {
        String date = "";

        try {
            //2017/04/09 19:51:10
            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            SimpleDateFormat dateformt = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            date = dateformt.format(Date.parse(currentDateTimeString));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    public static String CheckString(String str) {
        if (str == null) {
            return "";
        } else if (str.trim().equalsIgnoreCase("null") || str.equalsIgnoreCase("null")) {
            return "";
        } else if (str.length() > 0) {
            return str;
        } else {
            return "-";
        }

    }


    public static void SnackBar(Context ctx, String Message, View parentLayout, int id) {

        Snackbar mSnackBar = Snackbar.make(parentLayout, Message, Snackbar.LENGTH_LONG);
        View view = mSnackBar.getView();
        view.setPadding(5, 5, 5, 5);

        if (id == 1)//Positive
        {
            view.setBackgroundColor(ctx.getResources().getColor(R.color.colorPrimary));
        } else if (id == 2)//Negative
        {
            view.setBackgroundColor(ctx.getResources().getColor(R.color.md_deep_orange_300));
        } else//Negative
        {
            view.setBackgroundColor(ctx.getResources().getColor(R.color.md_red_400));
        }


        TextView mainTextView = (TextView) (view).findViewById(android.support.design.R.id.snackbar_text);
        mainTextView.setAllCaps(true);
        mainTextView.setTextSize(16);
        mainTextView.setTextColor(ctx.getResources().getColor(R.color.md_white_1000));
        mSnackBar.setDuration(4000);
        mSnackBar.show();


    }

    public static void ExitSweetDialog(final Context ctx, final Class<?> className) {

        new GenericDialog(ctx)
                .setLayoutColor(R.color.md_green_500)
                .setImage(R.drawable.ic_exit_to_app_black_24dp)
                .setTitle(ctx.getResources().getString(R.string.message_title))
                .setDescription(ctx.getResources().getString(R.string.are_you_sure_want_to_exit))
                .setPossitiveButtonTitle("YES")
                .setNegativeButtonTitle("NO")
                .setOnPossitiveListener(new GenericDialog.possitiveOnClick() {
                    @Override
                    public void onPossitivePerformed() {
                        ((Activity) ctx).finishAffinity();
                    }
                });


    }


    public static void animation1(ImageView mLogo) {

        ObjectAnimator scaleXAnimation = ObjectAnimator.ofFloat(mLogo, "scaleX", 5.0F, 1.0F);
        scaleXAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        scaleXAnimation.setDuration(1200);
        ObjectAnimator scaleYAnimation = ObjectAnimator.ofFloat(mLogo, "scaleY", 5.0F, 1.0F);
        scaleYAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        scaleYAnimation.setDuration(1200);
        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(mLogo, "alpha", 0.0F, 1.0F);
        alphaAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        alphaAnimation.setDuration(1200);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(scaleXAnimation).with(scaleYAnimation).with(alphaAnimation);
        animatorSet.setStartDelay(500);
        animatorSet.start();

    }



}//END
