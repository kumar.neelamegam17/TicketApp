package ticket.ispider.com.CoreModules;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import ticket.ispider.com.R;
import ticket.ispider.com.Utility.Tools;

/**
 * MUTHUKUMAR N
 * kumar.neelamegam17@gmail.com
 * Android Developer
 * 24-7-2018
 * Created a constants class
 */

public class Splash extends CoreActivity {

    ImageView imgvwLogo;
    TextView txtvwAppname;

    ProgressBar progressBar;
    TextView progress_status;

    private int progress = 0;
    private int progressStatus = 0;
    private final Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        try {
            isStoragePermissionGranted();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onPermissionsGranted(int requestCode) {

        try {

            GET_INITIALIZE();

        } catch (Exception e) {

            e.printStackTrace();
        }


    }

    @Override
    protected void bindViews() {

    }

    @Override
    protected void setListeners() {

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void GET_INITIALIZE() {


        try {
            imgvwLogo = findViewById(R.id.imgvw_logo);
            txtvwAppname = findViewById(R.id.txtvw_appname);
            progressBar = findViewById(R.id.progressBar);
            progress_status = findViewById(R.id.textprgrs);

            Constant.animation1(imgvwLogo);

            CallNextIntent();
            // for system bar in lollipop
            Tools.systemBarLolipop(this);
        } catch (Exception e) {

        }
    }

    private void CallNextIntent() {

        new Thread(new Runnable() {
            public void run() {

                while (progressStatus < 100) {
                    progressStatus = doSomeWork();

                    handler.post(() -> {
                        progressBar.setProgress(progressStatus);
                        progress_status.setText((String.valueOf(progressStatus))+" %");
                    });
                }

                handler.post(() -> {

                    progressBar.setVisibility(View.GONE);
                    Constant.globalStartIntent(Splash.this, Login.class, null);

                });
            }

            private int doSomeWork() {
                try {
                    // ---simulate doing some work---
                    Thread.sleep(30L);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ++progress;
                return
                        progress;
            }
        }).start();

    }


}//end

