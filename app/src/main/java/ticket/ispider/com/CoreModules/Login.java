package ticket.ispider.com.CoreModules;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;

import com.balysv.materialripple.MaterialRippleLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import ticket.ispider.com.CoreModules.Coordinator.MenuDrawer_Coordinator;
import ticket.ispider.com.CoreModules.Field_Engineer.MenuDrawer_FieldEngineer;
import ticket.ispider.com.CoreModules.Team_Leader.MenuDrawer_TeamLeader;
import ticket.ispider.com.R;
import ticket.ispider.com.Utility.Tools;

public class Login extends AppCompatActivity {


    @BindView(R.id.edt_username)
    TextInputEditText edtUsername;
    @BindView(R.id.edt_password)
    TextInputEditText edtPassword;
    @BindView(R.id.chkbx_rememberme)
    AppCompatCheckBox chkbxRememberme;
    @BindView(R.id.button_login)
    MaterialRippleLayout buttonLogin;

    String USER_TL = "venkataramana.a@intechindia.com";//TL
    String USER_COORDINATOR = "sheela.r@intechindia.com";//Coordinator
    String USER_FIELDENGINEER = "sathish.r@intechindia.com";//Field Engineer


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_card_light);


        try {
            GET_INITIALIZE();
            CONTROLLISTENERS();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void CONTROLLISTENERS() {

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtUsername.getText().toString().equalsIgnoreCase("1"))//TL
                {
                    Constant.globalStartIntent(Login.this, MenuDrawer_TeamLeader.class, null);

                } else if (edtUsername.getText().toString().equalsIgnoreCase("2")) {
                    Constant.globalStartIntent(Login.this, MenuDrawer_Coordinator.class, null);

                } else if (edtUsername.getText().toString().equalsIgnoreCase("3")) {
                    Constant.globalStartIntent(Login.this, MenuDrawer_FieldEngineer.class, null);

                }

            }
        });

    }

    private void GET_INITIALIZE() {

        ButterKnife.bind(this);

        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        //Snackbar.make(parent_view, "Forgot Password", Snackbar.LENGTH_SHORT).show();


    }


    @Override
    public void onBackPressed() {
        Constant.ExitSweetDialog(Login.this, MenuDrawer_FieldEngineer.class);
    }

}//END
