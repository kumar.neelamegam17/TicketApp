package ticket.ispider.com.CoreModules.Coordinator;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import ticket.ispider.com.CoreModules.Constant;
import ticket.ispider.com.R;

public class Masters extends AppCompatActivity {

    //**********************************************************************************************
    private ActionBar actionBar;
    private Toolbar toolbar;

    @BindView(R.id.layout_master_department)
    LinearLayout layout_master_department;

    @BindView(R.id.layout_master_designation)
    LinearLayout layout_master_designation;

    @BindView(R.id.layout_master_location)
    LinearLayout layout_master_location;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_masters);

        try {
            GET_INITIALIZE();
            CONTROLLISTENERS();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void CONTROLLISTENERS() {


        layout_master_department.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Load_Master_Department();

            }
        });


        layout_master_designation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Load_Master_Designation();

            }
        });


        layout_master_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Load_Master_Location();
            }
        });

    }

    private void GET_INITIALIZE() {


        try {
            ButterKnife.bind(this);
            initToolbar();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //**********************************************************************************************
    private void initToolbar() {
        try {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle("Masters");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //**********************************************************************************************
    @Override
    public void onBackPressed() {

        this.finish();

    }

    //**********************************************************************************************

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    public void Load_Master_Department() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Masters.this);
        LayoutInflater inflater = Masters.this.getLayoutInflater();
        View inflatedLayout = inflater.inflate(R.layout.screen_master_department, null);

        AlertDialog show;


        // Set the dialog layout
        builder.setView(inflatedLayout);
        show = builder.show();
        show.setCancelable(false);
        show.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation2;


        LinearLayout parentLayout;
        LinearLayout lytParent;
        TextInputEditText edtDeptcode;
        TextInputEditText edtDeptname;
        MaterialRippleLayout buttonCancel;
        MaterialRippleLayout buttonSave;

        parentLayout = (LinearLayout) inflatedLayout.findViewById(R.id.parent_layout);
        lytParent = (LinearLayout) inflatedLayout.findViewById(R.id.lyt_parent);
        edtDeptcode = (TextInputEditText) inflatedLayout.findViewById(R.id.edt_deptcode);
        edtDeptname = (TextInputEditText) inflatedLayout.findViewById(R.id.edt_deptname);
        buttonCancel = (MaterialRippleLayout) inflatedLayout.findViewById(R.id.button_cancel);
        buttonSave = (MaterialRippleLayout) inflatedLayout.findViewById(R.id.button_save);


        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
            }
        });


    }

    public void Load_Master_Designation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Masters.this);
        LayoutInflater inflater = Masters.this.getLayoutInflater();
        View inflatedLayout = inflater.inflate(R.layout.screen_master_designation, null);

        AlertDialog show;

        // Set the dialog layout
        builder.setView(inflatedLayout);
        show = builder.show();
        show.setCancelable(false);
        show.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation2;

        LinearLayout parentLayout;
        LinearLayout lytParent;
        TextInputEditText edtDesignation;
        MaterialRippleLayout buttonCancel;
        MaterialRippleLayout buttonSave;

        parentLayout = (LinearLayout) inflatedLayout.findViewById(R.id.parent_layout);
        lytParent = (LinearLayout) inflatedLayout.findViewById(R.id.lyt_parent);
        edtDesignation = (TextInputEditText) inflatedLayout.findViewById(R.id.edt_designation);
        buttonCancel = (MaterialRippleLayout) inflatedLayout.findViewById(R.id.button_cancel);
        buttonSave = (MaterialRippleLayout) inflatedLayout.findViewById(R.id.button_save);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
            }
        });


    }

    public void Load_Master_Location() {

        AlertDialog.Builder builder = new AlertDialog.Builder(Masters.this);
        LayoutInflater inflater = Masters.this.getLayoutInflater();
        View inflatedLayout = inflater.inflate(R.layout.screen_master_location, null);

        AlertDialog show;

        // Set the dialog layout
        builder.setView(inflatedLayout);
        show = builder.show();
        show.setCancelable(false);
        show.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation2;

        LinearLayout parentLayout;
        LinearLayout lytParent;
        TextInputEditText edtLocation;
        MaterialRippleLayout buttonCancel;
        MaterialRippleLayout buttonSave;


        parentLayout = (LinearLayout) inflatedLayout.findViewById(R.id.parent_layout);
        lytParent = (LinearLayout) inflatedLayout.findViewById(R.id.lyt_parent);
        edtLocation = (TextInputEditText) inflatedLayout.findViewById(R.id.edt_location);
        buttonCancel = (MaterialRippleLayout) inflatedLayout.findViewById(R.id.button_cancel);
        buttonSave = (MaterialRippleLayout) inflatedLayout.findViewById(R.id.button_save);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
            }
        });


    }


}//END
