package ticket.ispider.com.CoreModules.Team_Leader;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ticket.ispider.com.CoreModules.Field_Engineer.My_Tickets;
import ticket.ispider.com.CoreModules.Generic_DataObjects;
import ticket.ispider.com.R;
import ticket.ispider.com.Utility.GenericRecyclerAdapter;

public class Ticket_Assign extends AppCompatActivity {

    //**********************************************************************************************
    private ActionBar actionBar;
    private Toolbar toolbar;
    @BindView(R.id.rcylr_list)
    RecyclerView rcylrview;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_mytickets_main);
       // setContentView(R.layout.screen_ticket_assign);

        try {
            GET_INITIALIZE();
            CONTROLLISTENERS();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void CONTROLLISTENERS() {

        rcylrview.setLayoutManager(new LinearLayoutManager(Ticket_Assign.this));
        rcylrview.setNestedScrollingEnabled(false);


        datalist=new ArrayList<>();
        Generic_DataObjects.Ticket_Assign value= new Generic_DataObjects.Ticket_Assign();

        for (int i = 0; i < 10; i++) {

            value= new Generic_DataObjects.Ticket_Assign();
            value.setId(i+1);
            value.setPriority("2");
            value.setProblemReported("hello test");
            value.setServerId("2");
            value.setTicketNo("1123");
            value.setTicketStatus("Opened");
            value.setTicketDate("02-09-2018");
            datalist.add(value);
        }

        LoadList();

    }

    //Recycler data
    GenericRecyclerAdapter RecyclerAdapter;
    List<Generic_DataObjects.Ticket_Assign> datalist = new ArrayList<>();

    private void LoadList() {


        RecyclerAdapter = new GenericRecyclerAdapter(datalist, R.layout.rowitem_ticket_assign_carditem)
                .setRowItemView(new GenericRecyclerAdapter.AdapterView() {
                    @Override
                    public Object setAdapterView(ViewGroup parent, int viewType, int layoutId) {
                        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
                    }


                    @Override
                    public void onBindView(Object holder, final int position, Object data, final List<Object> dataList) {

                        ViewHolder myViewHolders = (ViewHolder) holder;
                        final Generic_DataObjects.Ticket_Assign value = (Generic_DataObjects.Ticket_Assign) data;

                        try {

                           myViewHolders.txtvwSno.setText(String.valueOf(value.getId()));
                           myViewHolders.txtvwTicketstatus.setText(value.getTicketStatus());
                           myViewHolders.txtvwTicketno.setText(value.getTicketNo());
                           myViewHolders.txtvwTicketdate.setText(value.getTicketDate());
                           myViewHolders.txtvwProblemReported.setText(value.getProblemReported());
                           myViewHolders.txtvwPriority.setText(value.getPriority());

                            myViewHolders.imgvwOptions.setVisibility(View.GONE);
                            myViewHolders.imgvwOptions.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    PopupMenu popupMenu = new PopupMenu(Ticket_Assign.this, v);
                                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                        @Override
                                        public boolean onMenuItemClick(MenuItem item) {

                                            if(item.getTitle().toString().equalsIgnoreCase("Approve"))
                                            {

                                            }

                                            return true;
                                        }
                                    });
                                    popupMenu.inflate(R.menu.menu_ticket_approve_more);
                                    popupMenu.show();
                                }
                            });

                            myViewHolders.parentLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    AlertDialog.Builder builder = new AlertDialog.Builder(Ticket_Assign.this);
                                    LayoutInflater inflater = Ticket_Assign.this.getLayoutInflater();
                                    View inflatedLayout = inflater.inflate(R.layout.screen_ticket_assign, null);

                                    AlertDialog show;

                                    // Set the dialog layout
                                    builder.setView(inflatedLayout);
                                    show = builder.show();
                                    show.setCancelable(false);
                                    show.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation2;

                                    MaterialRippleLayout buttonCancel = (MaterialRippleLayout) inflatedLayout.findViewById(R.id.button_cancel);
                                    MaterialRippleLayout buttonSave = (MaterialRippleLayout) inflatedLayout.findViewById(R.id.button_save);


                                    buttonCancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            show.dismiss();
                                        }
                                    });

                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                });
        rcylrview.setAdapter(RecyclerAdapter);


    }




    public static class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout parentLayout;
        private LinearLayout lytParent;
        private TextView txtvwSno;
        private TextView txtvwTicketstatus;
        private TextView txtvwTicketno;
        private TextView txtvwTicketdate;
        private TextView txtvwProblemReported;
        private TextView txtvwPriority;
        private ImageView imgvwOptions;


        ViewHolder(View view) {
            super(view);

            parentLayout = (LinearLayout) view.findViewById(R.id.parent_layout);
            lytParent = (LinearLayout) view.findViewById(R.id.lyt_parent);
            txtvwSno = (TextView) view.findViewById(R.id.txtvw_sno);
            txtvwTicketstatus = (TextView) view.findViewById(R.id.txtvw_ticketstatus);
            txtvwTicketno = (TextView) view.findViewById(R.id.txtvw_ticketno);
            txtvwTicketdate = (TextView) view.findViewById(R.id.txtvw_ticketdate);
            txtvwProblemReported = (TextView) view.findViewById(R.id.txtvw_problem_reported);
            txtvwPriority = (TextView) view.findViewById(R.id.txtvw_priority);
            imgvwOptions = (ImageView) view.findViewById(R.id.imgvw_options);
        }

    }

    //**********************************************************************************************
    private void GET_INITIALIZE() {

        try {
            ButterKnife.bind(this);
            initToolbar();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //**********************************************************************************************
    private void initToolbar() {
        try {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle("Ticket Assign");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //**********************************************************************************************
    @Override
    public void onBackPressed() {

        this.finish();

    }

    //**********************************************************************************************

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();  return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //**********************************************************************************************
    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    //**********************************************************************************************
    @Override
    protected void onStop() {
        super.onStop();
    }

    //**********************************************************************************************
}//END