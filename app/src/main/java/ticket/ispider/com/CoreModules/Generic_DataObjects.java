package ticket.ispider.com.CoreModules;

public class Generic_DataObjects {


    public static class Ticket_View
    {

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getServerId() {
            return ServerId;
        }

        public void setServerId(String serverId) {
            ServerId = serverId;
        }

        public String getTicketNo() {
            return TicketNo;
        }

        public void setTicketNo(String ticketNo) {
            TicketNo = ticketNo;
        }

        public String getTicketDate() {
            return TicketDate;
        }

        public void setTicketDate(String ticketDate) {
            TicketDate = ticketDate;
        }

        public String getComplaint() {
            return Complaint;
        }

        public void setComplaint(String complaint) {
            Complaint = complaint;
        }

        public String getCustomer() {
            return Customer;
        }

        public void setCustomer(String customer) {
            Customer = customer;
        }

        int Id;
        String ServerId;
        String TicketNo;
        String TicketDate;
        String Complaint;
        String Customer;


    }



    public static class My_Tickets
    {
        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getTicketNumber() {
            return TicketNumber;
        }

        public void setTicketNumber(String ticketNumber) {
            TicketNumber = ticketNumber;
        }

        public String getTicketDate() {
            return TicketDate;
        }

        public void setTicketDate(String ticketDate) {
            TicketDate = ticketDate;
        }

        public String getComplaintDate() {
            return ComplaintDate;
        }

        public void setComplaintDate(String complaintDate) {
            ComplaintDate = complaintDate;
        }

        public String getCustomer() {
            return Customer;
        }

        public void setCustomer(String customer) {
            Customer = customer;
        }

        public String getPriority() {
            return Priority;
        }

        public void setPriority(String priority) {
            Priority = priority;
        }

        public String getProblemReported() {
            return ProblemReported;
        }

        public void setProblemReported(String problemReported) {
            ProblemReported = problemReported;
        }

        public String getServerId() {
            return ServerId;
        }

        public void setServerId(String serverId) {
            ServerId = serverId;
        }

        String ServerId;
        int Id;
        String TicketNumber;
        String TicketDate;
        String ComplaintDate;
        String Customer;
        String Priority;
        String ProblemReported;


    }



    public static class Ticket_Approve
    {

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getTicketNo() {
            return TicketNo;
        }

        public void setTicketNo(String ticketNo) {
            TicketNo = ticketNo;
        }

        public String getCallOpenDate() {
            return CallOpenDate;
        }

        public void setCallOpenDate(String callOpenDate) {
            CallOpenDate = callOpenDate;
        }

        public String getCallCloseDate() {
            return CallCloseDate;
        }

        public void setCallCloseDate(String callCloseDate) {
            CallCloseDate = callCloseDate;
        }

        public String getProblemReported() {
            return ProblemReported;
        }

        public void setProblemReported(String problemReported) {
            ProblemReported = problemReported;
        }

        public void setServerId(String serverId) {
            ServerId = serverId;
        }

        public String getServerId() {
            return ServerId;
        }

        String ServerId;
        int Id;
        String TicketNo;
        String CallOpenDate;
        String CallCloseDate;
        String ProblemReported;

    }



    public static class Ticket_Assign
    {
        String ServerId;
        int Id;
        String TicketStatus;
        String TicketNo;
        String TicketDate;
        String ProblemReported;
        String Priority;

        public String getServerId() {
            return ServerId;
        }

        public void setServerId(String serverId) {
            ServerId = serverId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getTicketStatus() {
            return TicketStatus;
        }

        public void setTicketStatus(String ticketStatus) {
            TicketStatus = ticketStatus;
        }

        public String getTicketNo() {
            return TicketNo;
        }

        public void setTicketNo(String ticketNo) {
            TicketNo = ticketNo;
        }

        public String getTicketDate() {
            return TicketDate;
        }

        public void setTicketDate(String ticketDate) {
            TicketDate = ticketDate;
        }

        public String getProblemReported() {
            return ProblemReported;
        }

        public void setProblemReported(String problemReported) {
            ProblemReported = problemReported;
        }

        public String getPriority() {
            return Priority;
        }

        public void setPriority(String priority) {
            Priority = priority;
        }
    }




}
