package ticket.ispider.com.CoreModules.Coordinator;

import butterknife.BindView;
import butterknife.ButterKnife;
import ticket.ispider.com.CoreModules.Constant;
import ticket.ispider.com.CoreModules.Login;
import ticket.ispider.com.CoreModules.Team_Leader.MenuDrawer_TeamLeader;
import ticket.ispider.com.R;
import ticket.ispider.com.Utility.CustomIntent;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


public class MenuDrawer_Coordinator extends AppCompatActivity {

    //**********************************************************************************************
    private ActionBar actionBar;
    private Toolbar toolbar;

    @BindView(R.id.layout_parent_dashboard)
    LinearLayout layoutParentDashboard;
    @BindView(R.id.txtvw_question_title)
    TextView txtvwQuestionTitle;
    @BindView(R.id.layout_total_tickets)
    LinearLayout layoutTotalTickets;
    @BindView(R.id.txtvw_approved_count)
    TextView txtvwApprovedCount;
    @BindView(R.id.layout_completed_tickets)
    LinearLayout layoutCompletedTickets;
    @BindView(R.id.txtvw_completed_tickets)
    TextView txtvwPendingCount;
    @BindView(R.id.layout_students)
    CardView layoutStudents;
    @BindView(R.id.txtvw_students_strength)
    TextView txtvwStudentsStrength;
    @BindView(R.id.layout_add_ticket)
    LinearLayout layoutAddTicket;
    @BindView(R.id.txtvw_addticket)
    TextView txtvwAddticket;
    @BindView(R.id.layout_view_ticket)
    LinearLayout layoutViewTicket;
    @BindView(R.id.txtvw_viewticket)
    TextView txtvwViewticket;
    @BindView(R.id.txtvw_addcustomer)
    TextView txtvwAddcustomer;
    @BindView(R.id.txtvw_masters)
    TextView txtvwMasters;

    //**********************************************************************************************

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_drawer_news);

        try {
            GET_INITIALIZE();
            CONTROLLISTENERS();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CONTROLLISTENERS() {

        layoutAddTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constant.globalStartIntent2(MenuDrawer_Coordinator.this, Ticket_Entry.class, null);

            }
        });

        layoutViewTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constant.globalStartIntent2(MenuDrawer_Coordinator.this, Ticket_View.class, null);

            }
        });

        txtvwAddcustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constant.globalStartIntent2(MenuDrawer_Coordinator.this, Customer_Entry.class, null);

            }
        });

        txtvwMasters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constant.globalStartIntent2(MenuDrawer_Coordinator.this, Masters.class, null);

            }
        });
    }


    private void GET_INITIALIZE() {

        try {
            ButterKnife.bind(this);

            initToolbar();

            initNavigationMenu();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //**********************************************************************************************
    private void initToolbar() {
        try {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle("Welcome Coordinator");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //**********************************************************************************************
    private void initNavigationMenu() {

        NavigationView nav_view = findViewById(R.id.nav_view);
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.open_drawer, R.string.close_drawer) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(item -> {
            Toast.makeText(getApplicationContext(), item.getTitle() + " Selected", Toast.LENGTH_SHORT).show();
           // actionBar.setTitle(item.getTitle());
            drawer.closeDrawers();

            if (item.getTitle().equals("Logout")) {

                Constant.globalStartIntent(MenuDrawer_Coordinator.this, Login.class, null);

            }

            return true;
        });

        // open drawer at start
        // drawer.openDrawer(GravityCompat.START);
    }
    //**********************************************************************************************

    @Override
    public void onBackPressed() {

        Constant.ExitSweetDialog(MenuDrawer_Coordinator.this, MenuDrawer_Coordinator.class);

    }

    //**********************************************************************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.dashboard_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.menu_exit) {
            Constant.ExitSweetDialog(MenuDrawer_Coordinator.this, MenuDrawer_Coordinator.class);

        }

        return super.onOptionsItemSelected(item);
    }


}//END
